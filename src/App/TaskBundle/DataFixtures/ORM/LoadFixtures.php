<?php

namespace App\TaskBundle\DataFixtures\ORM;

use App\TaskBundle\Entity\Product;
use App\TaskBundle\Entity\User;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadFixtures implements FixtureInterface, ContainerAwareInterface
{
    /** @var ContainerInterface */
    private $container;

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $this->loadUsers($manager);
        $this->loadProducts($manager);
    }

    private function loadUsers(ObjectManager $manager)
    {
        $passwordEncoder = $this->container->get('security.password_encoder');

        $svetlio = new User();
        $svetlio->setUsername('svetlio');
        $svetlio->setEmail('sfetliooo@gmail.com');
        $encodedPassword = $passwordEncoder->encodePassword($svetlio, 'parola123');
        $svetlio->setPassword($encodedPassword);
        $manager->persist($svetlio);


        $admin = new User();
        $admin->setUsername('admin');
        $admin->setEmail('admin@admin.com');
        $encodedPassword = $passwordEncoder->encodePassword($admin, 'admin123');
        $admin->setPassword($encodedPassword);
        $manager->persist($admin);

        $manager->flush();
    }

    private function loadProducts(ObjectManager $manager)
    {
        foreach (range(0, 12) as $i) {
            $product = new Product();
            
            $title = $this->getProductName($i);
            $product->setTitle($title);
            $product->setSlug($this->slugify($title));
            $product->setDescription($this->getDescription());
            $product->setRating(0);

            $product->setPublished(new \DateTime('now - '.rand(1,100).'days'));

            $manager->persist($product);
        }

        $manager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    private function getDescription()
    {
        return <<<'DESCRIPTION'
Lorem ipsum dolor sit amet consectetur adipisicing elit, sed do eiusmod tempor
incididunt ut labore et **dolore magna aliqua**: Duis aute irure dolor in
reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
deserunt mollit anim id est laborum.
DESCRIPTION;
    }

    private function getProductName($i)
    {
        $titles = [
            'Iphone 7', 'Iphone 8', 'Iphone X', 'Iphone XS',
            'Samsung Galaxy S6', 'Samsung Galaxy S7', 'Samsung Galaxy S8', 'Samsung Galaxy S9',
            'Xiaomi M3', 'Xiaomi M4', 'Xiaomi M5', 'Xiaomi M6',
            'Google Pixcel 1', 'Google Pixcel 2', 'Google Pixcel 3',
        ];

        return $titles[$i];
    }

    /** fast solution unstable */
    private function slugify($string)
    {
        return strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string)));
    }
}
