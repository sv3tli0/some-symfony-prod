<?php

namespace App\TaskBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ProductReview
 *
 * @ORM\Table(name="product_review")
 * @ORM\Entity(repositoryClass="App\TaskBundle\Repository\ProductReviewRepository")
 */
class ProductReview
{
    const STATUS_NOT_APPROVED = 0;
    const STATUS_APPROVED = 1;
    const STATUS_REJECTED = 2;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\Length(
     *     min="5",
     *     max="100",
     *     minMessage="Review title must be at least {{ limit }} symbols",
     *     maxMessage="Review title can be maximum {{ limit }} symbols"
     * )
     * @ORM\Column(name="title", type="string", length=150)
     */
    private $title;

    /**
     * @var string
     *
     * @Assert\Length(
     *     min="10", max="3000",
     *     minMessage="Description must be at least {{ limit }} symbols",
     *     maxMessage="Description can be maximum {{ limit }} symbols"
     * )
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var int
     *
     * @Assert\Range(
     *     min = 1, max = 5,
     *     minMessage = "Rating must be at least 1",
     *     maxMessage = "Rating must be maximum 5"
     * )
     * @ORM\Column(name="rating", type="smallint")
     */
    private $rating;

    /**
     * @var Product|null
     *
     * @ORM\ManyToOne(targetEntity="App\TaskBundle\Entity\Product", inversedBy="reviews")
     */
    private $product;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     * @Assert\DateTime
     */
    private $published;

    public function __construct()
    {
        $this->published = new \DateTime();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return ProductReview
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return ProductReview
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set rating
     *
     * @param integer $rating
     *
     * @return ProductReview
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return int
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set product
     *
     * @param Product|null $product
     *
     * @return ProductReview
     */
    public function setProduct(?Product $product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return Product|null
     */
    public function getProduct() : ?Product
    {
        return $this->product;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return ProductReview
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }


    /**
     * Set published
     *
     * @param \DateTime $published
     *
     * @return $this
     */
    public function setPublished(\DateTime $published)
    {
        $this->published = $published;

        return $this;
    }

    public function getPublished(): \DateTime
    {
        return $this->published;
    }

    public function getStatusTitle()
    {
        switch ($this->status) :
            case self::STATUS_APPROVED:
                return 'Approved';

            case self::STATUS_REJECTED:
                return 'Rejected';

            default:
            case self::STATUS_NOT_APPROVED:
                return 'Not approved';

        endswitch;
    }
}

