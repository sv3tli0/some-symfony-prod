<?php

namespace App\TaskBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Product
 *
 * @ORM\Table(name="product")
 * @ORM\Entity(repositoryClass="App\TaskBundle\Repository\ProductRepository")
 */
class Product
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\Length(
     *     min="5",
     *     max="100",
     *     minMessage="Product title must be at least {{ limit }} symbols",
     *     maxMessage="Product title can be maximum {{ limit }} symbols"
     * )
     * @ORM\Column(name="title", type="string", length=150)
     */
    private $title;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="ProductReview", mappedBy="product")
     */
    private $reviews;

    /**
     * @var string
     *
     *
     * @Assert\Length(
     *     min="5",
     *     max="100",
     *     minMessage="Product slug must be at least {{ limit }} symbols",
     *     maxMessage="Product slug can be maximum {{ limit }} symbols"
     * )
     * @Assert\Expression(
     *     expression="^[a-z0-9]+(?:-[a-z0-9]+)*$",
     *     message="Slug can have only lower letters, digits and dashes!"
     * )
     * @ORM\Column(name="slug", type="string", length=150)
     */
    private $slug;

    /**
     * @var string
     *
     * @Assert\Length(
     *     min="10", max="50000",
     *     minMessage="Description must be at least {{ limit }} symbols",
     *     maxMessage="Description can be maximum {{ limit }} symbols"
     * )
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="rating", type="decimal", precision=4, scale=2)
     */
    private $rating;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     * @Assert\DateTime
     */
    private $published;


    public function __construct()
    {
        $this->published = new \DateTime();
        $this->reviews = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Product
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Product
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    public function getReviews() : Collection
    {
        return $this->reviews;
    }

    public function getApprovedReviews(): Collection
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('status', ProductReview::STATUS_APPROVED));
        return $this->reviews->matching($criteria);

    }

    /**
     * Set published
     *
     * @param \DateTime $published
     *
     * @return $this
     */
    public function setPublished(\DateTime $published)
    {
        $this->published = $published;

        return $this;
    }

    public function getPublished(): \DateTime
    {
        return $this->published;
    }

    /**
     * Set rating
     *
     * @param float $rating
     *
     * @return Product
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return int
     */
    public function getRating()
    {
        return $this->rating;
    }
}

