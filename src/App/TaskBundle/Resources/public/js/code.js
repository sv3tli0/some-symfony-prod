'use strict';

(function ($) {
    $(document).on('change', '.approve-form input[type=radio]', function () {
        var elem = $(this).closest('.admin-review-box');
        var $val = $(this).val();

        $.post($(this).closest('form').attr('action'), {status: $val})
            .done(function (res) {
                if(res.hasOwnProperty('status')) {
                    elem.find('.status-title').text(res.status)
                        .removeClass('text-warning')
                        .removeClass('text-success')
                        .removeClass('text-danger');
                    if($val == 1) {
                        elem.find('.status-title').addClass('text-success');
                    } else if($val == 2) {
                        elem.find('.status-title').addClass('text-danger');
                    }
                } else if(res.hasOwnProperty('message')) {
                    alert('message');
                }
            });
    });

    $('.star-rating').each(function () {
        $(this).html('');
        addStars($(this), $(this).data('rating'));
    });

    function addStars($elem, rating = 0)
    {
        for(var i=1;i<=5;i++) {
            var star = $('<i class="" data-rating="'+i+'"></i>');
            if(rating >= i-0.25) {
                star.addClass('fas fa-star gold-star');
            } else  if(rating >= i-0.75) {
                star.addClass('fas fa-star-half-alt gold-star');
            } else {
                star.addClass('far fa-star');
            }
            $elem.append(star);
        }
    }

    $('input.input-star-rating').each(function () {
        $(this).attr('type','hidden');
        var $div = $('<div class="d-star-rating">');
        addStars($div);
        $(this).parent().append($div);
    });

    var ratingSelected = false;

    $(document).on('mouseover', '.d-star-rating i', function () {
        var par = $(this).parent();
        var rating = $(this).data('rating');
        if(ratingSelected == false) {
            par.parent().find('input').val(rating);
            setStars(par.find('i'), rating);
        }
    });

    $(document).on('click', '.d-star-rating i', function () {
        var par = $(this).parent();
        var rating = $(this).data('rating');
        par.parent().find('input').val(rating);
        setStars(par.find('i'), rating);
        ratingSelected = rating;
    });

    function setStars(elements, rating) {
        elements.each(function (i) {
            if(rating >= i+1) {
                $(this).removeClass('far fa-star');
                $(this).addClass('fas fa-star gold-star');
            } else {
                $(this).removeClass('fas fa-star gold-star');
                $(this).addClass('far fa-star');
            }
        });
    }

    })(jQuery);