<?php

namespace App\TaskBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class AuthController extends Controller
{
    /**
     * @Route("/login", name="auth_login")
     */
    public function loginAction()
    {
        if($this->getUser() && $this->getUser()->getId()) {
            return $this->redirectToRoute('homepage');
        }

        return $this->render('TaskBundle:Auth:login.html.twig', [
            'error' => $this->get('security.authentication_utils')->getLastAuthenticationError(),
        ]);
    }

    /**
     * @Route("/logout", name="auth_logout")
     */
    public function logoutAction()
    {
        throw new \Exception('No place to be!');
    }

}
