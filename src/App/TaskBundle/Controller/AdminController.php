<?php

namespace App\TaskBundle\Controller;

use App\TaskBundle\Entity\Product;
use App\TaskBundle\Entity\ProductReview;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminController
 * @package App\TaskBundle\Controller
 *
 * @Route(path="admin")
 */
class AdminController extends Controller
{
    /**
     * @Route("/", name="admin_dashboard")
     */
    public function indexAction()
    {
        $search = [];
        if(is_numeric($this->get('request')->get('status')) && in_array($this->get('request')->get('status'), [
            ProductReview::STATUS_NOT_APPROVED, ProductReview::STATUS_APPROVED, ProductReview::STATUS_REJECTED
            ])) {
            $search['status']['eq'] = intval($this->get('request')->get('status'));
        }
        return $this->render('TaskBundle:Admin:reviews.html.twig', [
            'reviews' => $this->getDoctrine()->getRepository(ProductReview::class)->findBy($search,['id'=>'desc'])
        ]);
    }

    /**
     * @Route("/review/{review}/status", name="admin_review_status", methods={"POST"}, condition="request.isXmlHttpRequest()")
     */
    public function reviewStatusAction(Request $request, ProductReview $review)
    {
        $status = intval($request->get('status'));
        if(in_array($status, [ProductReview::STATUS_APPROVED, ProductReview::STATUS_REJECTED])) {
            $review->setStatus($status);
            $this->getDoctrine()->getManager()->persist($review);
            $this->getDoctrine()->getManager()->flush();
            if($status == ProductReview::STATUS_APPROVED) {
                $this->getDoctrine()->getRepository(ProductReview::class)->updateProductRating($review);
            }
            return JsonResponse::create([
                'success' => true,
                'status' => $review->getStatusTitle(),
            ]);
        }

        return JsonResponse::create([
            'success' => false,
            'message' => 'Unable to change Review Status!'
        ]);
    }
}
