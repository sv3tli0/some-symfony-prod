<?php

namespace App\TaskBundle\Controller;

use App\TaskBundle\Entity\Product;
use App\TaskBundle\Entity\ProductReview;
use App\TaskBundle\Form\ProductReviewType;
use Doctrine\DBAL\ConnectionException;
use Doctrine\DBAL\Driver\PDOException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $products = [];
        if ($this->getDoctrine()->getConnection()->getSchemaManager()->tablesExist(['product']) == true) {
            $products = $this->getDoctrine()->getRepository(Product::class)->findAll();
        }

        return $this->render('TaskBundle:Default:index.html.twig', [
            'products' => $products
        ]);
    }

    /**
     * @Route("/product/{productSlug}", name="view_product")
     * @ParamConverter("product", options={"mapping": {"productSlug": "slug"}})
     */
    public function viewProductAction(Request $request, Product $product)
    {
        $reviewForm = $this->createForm(ProductReviewType::class);
        $reviewForm->handleRequest($request);

        if ($reviewForm->isSubmitted() && $reviewForm->isValid()) {
            $review = $reviewForm->getData();
            $review->setProduct($product);
            $review->setStatus(ProductReview::STATUS_NOT_APPROVED);
            $review->setPublished(new \DateTime());

            $this->getDoctrine()->getManager()->persist($review);
            $this->getDoctrine()->getManager()->flush();

            $this->get('session')->getFlashBag()->add('success', 'Your review has been added and it is waiting approve!');
            $reviewForm = $this->createForm(ProductReviewType::class);
        }

        return $this->render('TaskBundle:Default:view_product.html.twig', [
            'product' => $product,
            'form' => $reviewForm->createView()
        ]);
    }
}
